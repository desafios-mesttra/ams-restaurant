package com.orlandoxavier.restaurant.controller;

import com.orlandoxavier.restaurant.model.Order;
import com.orlandoxavier.restaurant.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/orders")
@RequiredArgsConstructor
@RestController
public class OrderController {

    final OrderService orderService;

    @PostMapping("/add")
    public String publishOrder(@RequestBody Order order) {
        return orderService.publishOrder(order);
    }

    @GetMapping
    public List<Order> all() {
        return orderService.all();
    }

    @GetMapping("/count")
    public Long count() {
        return orderService.count();
    }

    @GetMapping("/{id}")
    public Order get(@PathVariable("id") String id) {
        return orderService.get(id);
    }

    @PostMapping
    public Order save(@RequestBody Order order) {
        return orderService.save(order);
    }
}
