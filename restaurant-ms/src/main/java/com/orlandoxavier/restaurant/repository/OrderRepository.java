package com.orlandoxavier.restaurant.repository;

import com.orlandoxavier.restaurant.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

@Repository
@EnableMongoRepositories
public interface OrderRepository extends MongoRepository<Order, String> {
}
