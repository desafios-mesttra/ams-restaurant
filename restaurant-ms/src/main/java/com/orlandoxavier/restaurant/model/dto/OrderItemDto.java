package com.orlandoxavier.restaurant.model.dto;

import com.orlandoxavier.restaurant.model.OrderItem;
import com.orlandoxavier.restaurant.model.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class OrderItemDto {

    private String orderId;
    private List<OrderItem> orderItems;
    private OrderStatus orderStatus;
}