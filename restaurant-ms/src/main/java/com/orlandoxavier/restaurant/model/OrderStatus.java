package com.orlandoxavier.restaurant.model;

import java.io.Serializable;

public enum OrderStatus implements Serializable {
    PREPARING("Preparando"),
    DONE("Pronto");

    private String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}