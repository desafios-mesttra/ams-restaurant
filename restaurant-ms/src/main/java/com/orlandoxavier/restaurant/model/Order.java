package com.orlandoxavier.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "order")
public class Order implements Serializable {

    @Id
    private String id;
    private String waiter;
    private int table;

    @Transient
    private List<OrderItem> barItens;
    @Transient
    private List<OrderItem> kitchenItens;
    @Transient
    private OrderStatus statusBar;
    @Transient
    private OrderStatus statusKitchen;
}
