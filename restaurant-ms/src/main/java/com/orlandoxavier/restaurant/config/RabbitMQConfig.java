package com.orlandoxavier.restaurant.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String BAR_QUEUE = "order-bar-queue";
    public static final String KITCHEN_QUEUE = "order-kitchen-queue";
    public static final String EXCHANGE = "restaurant";
    public static final String BAR_ROUTING_KEY = "bar-order-routingKey";
    public static final String KITCHEN_ROUTING_KEY = "kitchen-order-routingKey";

    @Bean
    public Queue barQueue() {
        return new Queue(BAR_QUEUE);
    }

    @Bean
    public Queue kitchenQueue() {
        return new Queue(KITCHEN_QUEUE);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    public Binding bindingBar(@Qualifier("barQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(BAR_ROUTING_KEY);
    }

    @Bean
    public Binding bindingKitchen(@Qualifier("kitchenQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(KITCHEN_ROUTING_KEY);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter());
        return template;
    }
}
