package com.orlandoxavier.restaurant.service;

import com.orlandoxavier.restaurant.config.RabbitMQConfig;
import com.orlandoxavier.restaurant.model.Order;
import com.orlandoxavier.restaurant.model.OrderStatus;
import com.orlandoxavier.restaurant.model.dto.OrderItemDto;
import com.orlandoxavier.restaurant.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class OrderService {

    final OrderRepository orderRepository;
    final RabbitTemplate rabbitTemplate;

    public String publishOrder(Order order) {
        boolean hasBarItens = true;
        boolean hasKitchenItens = true;

        hasBarItens = order.getBarItens() != null && !order.getBarItens().isEmpty();
        hasKitchenItens = order.getKitchenItens() != null && !order.getKitchenItens().isEmpty();

        if (!hasBarItens && !hasKitchenItens) {
            return "The order should have items.";
        }

        order.setId(UUID.randomUUID().toString());

        if (hasBarItens) {
            rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE,
                    RabbitMQConfig.BAR_ROUTING_KEY,
                    getBarData(order));
        } else {
            order.setStatusBar(OrderStatus.DONE);
        }

        if (hasKitchenItens) {
            rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE,
                    RabbitMQConfig.KITCHEN_ROUTING_KEY,
                    getKitchenOrder(order));
        } else {
            order.setStatusKitchen(OrderStatus.DONE);
        }

        order = save(order);
        if (order != null) {
            return "The order was sent. :)";
        }
        return "Houston, we have a problem! The order was not sent.";
    }

    @CacheEvict(cacheNames = "orders", allEntries = true)
    public Order save(Order order) {
        if (order.getId() == null) {
            order.setId(UUID.randomUUID().toString());
        }
        Order saved = orderRepository.save(order);
        return saved;
    }

    @Cacheable(cacheNames = "orders")
    public List<Order> all() {
        return orderRepository.findAll();
    }

    public Long count() {
        return orderRepository.count();
    }

    public Order get(String id) {
        return orderRepository.findById(id).orElse(null);
    }

    private OrderItemDto getBarData(Order order) {
        return new OrderItemDto(order.getId(), order.getBarItens(), order.getStatusBar());
    }

    private OrderItemDto getKitchenOrder(Order order) {
        return new OrderItemDto(order.getId(), order.getKitchenItens(), order.getStatusBar());
    }
}
