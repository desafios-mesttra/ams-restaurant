package com.orlandoxavier.consumer.kitchen.service;

import com.orlandoxavier.consumer.kitchen.config.RabbitMQConfig;
import com.orlandoxavier.consumer.kitchen.model.Order;
import com.orlandoxavier.consumer.kitchen.model.OrderItem;
import com.orlandoxavier.consumer.kitchen.model.OrderStatus;
import com.orlandoxavier.consumer.kitchen.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class KitchenOrderService {

    final OrderRepository orderRepository;
    final RabbitTemplate rabbitTemplate;

    public Order get(String id) {
        return orderRepository.findById(id).orElse(null);
    }

    public void save(Order order) {
        for (OrderItem item : order.getOrderItems()) {
            if (item.getId() == null) {
                item.setId(UUID.randomUUID().toString());
            }
        }
        orderRepository.save(order);
        System.out.println("Order [" + order.getId() + "] saved.");
    }

    public ResponseEntity<?> makeDone(String id) {
        Optional<Order> order = orderRepository.findById(id);

        if (!order.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Order not found.");
        }

        if (order.get().getStatusKitchen().equals(OrderStatus.DONE)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Order has been done.");
        }

        order.get().setStatusKitchen(OrderStatus.DONE);
        orderRepository.save(order.get());
        sendKitchenDoneQueueMessage(order.get());
        sendNotificationQueueMessage(order.get());

        return ResponseEntity.status(204).body("Status alterado.");
    }

    public void sendKitchenDoneQueueMessage(Order order) {
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE,
                RabbitMQConfig.KITCHEN_DONE_ROUTING_KEY,
                order);
    }

    public void sendNotificationQueueMessage(Order order) {
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE,
                RabbitMQConfig.NOTIFICATION_ROUTING_KEY,
                order);
    }
}
