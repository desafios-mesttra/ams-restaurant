package com.orlandoxavier.consumer.kitchen.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String KITCHEN_QUEUE = "order-kitchen-queue";
    public static final String KITCHEN_DONE_QUEUE = "kitchen-done-queue";
    public static final String NOTIFICATION_QUEUE = "notification-queue";
    public static final String EXCHANGE = "restaurant";
    public static final String KITCHEN_ROUTING_KEY = "kitchen-order-routingKey";
    public static final String KITCHEN_DONE_ROUTING_KEY = "kitchen-done-routingKey";
    public static final String NOTIFICATION_ROUTING_KEY = "notification-routingKey";

    @Bean
    public Queue kitchenQueue() {
        return new Queue(KITCHEN_QUEUE);
    }

    @Bean
    public Queue kitchenDoneQueue() {
        return new Queue(KITCHEN_DONE_QUEUE);
    }

    @Bean
    public Queue notificationQueue() {
        return new Queue(NOTIFICATION_QUEUE);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    public Binding bindingKitchen(@Qualifier("kitchenQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(KITCHEN_ROUTING_KEY);
    }

    @Bean
    public Binding bindingKitchenDone(@Qualifier("kitchenDoneQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(KITCHEN_DONE_ROUTING_KEY);
    }

    @Bean
    public Binding bindingNotification(@Qualifier("notificationQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(NOTIFICATION_ROUTING_KEY);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter());
        return template;
    }
}
