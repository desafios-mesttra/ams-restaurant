package com.orlandoxavier.consumer.kitchen.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderItem implements Serializable {

    private String id;
    private String name;
    private int quantity;
    private String note;

}
