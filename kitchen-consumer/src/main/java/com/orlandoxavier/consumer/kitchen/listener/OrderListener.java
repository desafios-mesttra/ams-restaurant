package com.orlandoxavier.consumer.kitchen.listener;

import com.orlandoxavier.consumer.kitchen.config.RabbitMQConfig;
import com.orlandoxavier.consumer.kitchen.model.Order;
import com.orlandoxavier.consumer.kitchen.model.OrderStatus;
import com.orlandoxavier.consumer.kitchen.repository.OrderRepository;
import com.orlandoxavier.consumer.kitchen.service.KitchenOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@RequiredArgsConstructor
@Component
public class OrderListener {

    final OrderRepository orderRepository;
    final KitchenOrderService orderService;

    @RabbitListener(queues = RabbitMQConfig.KITCHEN_QUEUE)
    public void listener(Order order) {
        handleOrder(order);
    }

    private void handleOrder(Order order) {
        if (order.getStatusKitchen() == null) {
            return;
        }

        if (order.getStatusKitchen().getValue().equals(OrderStatus.DONE.getValue())) {
            orderService.sendNotificationQueueMessage(order);
        } else {
            orderService.save(order);
        }
    }
}
