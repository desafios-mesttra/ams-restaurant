package com.orlandoxavier.consumer.kitchen.controller;

import com.orlandoxavier.consumer.kitchen.model.Order;
import com.orlandoxavier.consumer.kitchen.service.KitchenOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/kitchen-orders")
public class OrderController {

    final KitchenOrderService orderService;

    @GetMapping("/{id}")
    public Order get(@PathVariable String id) {
        return orderService.get(id);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> makeDone(@PathVariable String id) {
        return orderService.makeDone(id);
    }
}
