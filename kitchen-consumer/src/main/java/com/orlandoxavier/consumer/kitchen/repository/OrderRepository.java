package com.orlandoxavier.consumer.kitchen.repository;

import com.orlandoxavier.consumer.kitchen.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

@Repository
@EnableMongoRepositories
public interface OrderRepository extends MongoRepository<Order, String> {
}
