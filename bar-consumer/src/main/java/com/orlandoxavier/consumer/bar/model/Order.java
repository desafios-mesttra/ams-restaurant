package com.orlandoxavier.consumer.bar.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "bar-order")
public class Order implements Serializable {

    @Id
    @JsonProperty("orderId")
    private String id;
    private List<OrderItem> orderItems;
    @JsonProperty("orderStatus")
    private OrderStatus statusBar;
}
