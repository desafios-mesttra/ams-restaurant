package com.orlandoxavier.consumer.bar.controller;

import com.orlandoxavier.consumer.bar.model.Order;
import com.orlandoxavier.consumer.bar.service.BarOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/bar-orders")
public class OrderController {

    final BarOrderService orderService;

    @GetMapping("/{id}")
    public Order get(@PathVariable String id) {
        return orderService.get(id);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> makeDone(@PathVariable String id) {
        return orderService.makeDone(id);
    }
}
