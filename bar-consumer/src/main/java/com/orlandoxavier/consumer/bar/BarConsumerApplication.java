package com.orlandoxavier.consumer.bar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BarConsumerApplication.class, args);
    }

}
