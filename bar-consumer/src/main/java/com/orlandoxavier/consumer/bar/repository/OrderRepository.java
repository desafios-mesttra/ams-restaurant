package com.orlandoxavier.consumer.bar.repository;

import com.orlandoxavier.consumer.bar.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

@Repository
@EnableMongoRepositories
public interface OrderRepository extends MongoRepository<Order, String> {
}
