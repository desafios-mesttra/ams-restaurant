package com.orlandoxavier.consumer.bar.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String BAR_QUEUE = "order-bar-queue";
    public static final String BAR_DONE_QUEUE = "bar-done-queue";
    public static final String NOTIFICATION_QUEUE = "notification-queue";
    public static final String EXCHANGE = "restaurant";
    public static final String BAR_ROUTING_KEY = "bar-order-routingKey";
    public static final String BAR_DONE_ROUTING_KEY = "bar-done-routingKey";
    public static final String NOTIFICATION_ROUTING_KEY = "notification-routingKey";

    @Bean
    public Queue barQueue() {
        return new Queue(BAR_QUEUE);
    }

    @Bean
    public Queue barDoneQueue() {
        return new Queue(BAR_DONE_QUEUE);
    }

    @Bean
    public Queue notificationQueue() {
        return new Queue(NOTIFICATION_QUEUE);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    public Binding bindingBar(@Qualifier("barQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(BAR_ROUTING_KEY);
    }

    @Bean
    public Binding bindingBarDone(@Qualifier("barDoneQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(BAR_DONE_ROUTING_KEY);
    }

    @Bean
    public Binding bindingNotification(@Qualifier("notificationQueue") Queue queue, TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(NOTIFICATION_ROUTING_KEY);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(messageConverter());
        return template;
    }
}
