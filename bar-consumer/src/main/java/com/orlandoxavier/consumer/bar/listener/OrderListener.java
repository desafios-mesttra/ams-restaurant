package com.orlandoxavier.consumer.bar.listener;

import com.orlandoxavier.consumer.bar.repository.OrderRepository;
import com.orlandoxavier.consumer.bar.config.RabbitMQConfig;
import com.orlandoxavier.consumer.bar.model.Order;
import com.orlandoxavier.consumer.bar.model.OrderStatus;
import com.orlandoxavier.consumer.bar.service.BarOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


@RequiredArgsConstructor
@Component
public class OrderListener {

    final OrderRepository orderRepository;
    final BarOrderService orderService;

    @RabbitListener(queues = RabbitMQConfig.BAR_QUEUE)
    public void listener(Order order) {
        handleOrder(order);
    }

    private void handleOrder(Order order) {
        if (order.getStatusBar() == null) {
            return;
        }

        if (order.getStatusBar().getValue().equals(OrderStatus.DONE.getValue())) {
            orderService.sendNotificationQueueMessage(order);
        } else {
            orderService.save(order);
        }
    }
}
