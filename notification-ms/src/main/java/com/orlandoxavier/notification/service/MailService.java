package com.orlandoxavier.notification.service;

import com.orlandoxavier.notification.model.Email;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class MailService {

    final JavaMailSender mailSender;
    final Environment env;

    private static final String fromProperty = "spring.mail.username";
    private static final String toProperty = "ams.restaurant.owner-email";

    public void sendMail(Email email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(env.getProperty(fromProperty));
        message.setTo(env.getProperty(toProperty));
        message.setSubject(email.getSubject());
        message.setText(email.getText());

        mailSender.send(message);
    }
}
