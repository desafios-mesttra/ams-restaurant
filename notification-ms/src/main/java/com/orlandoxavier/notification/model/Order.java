package com.orlandoxavier.notification.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Order implements Serializable {

    @JsonProperty("orderId")
    private String id;
    private List<OrderItem> orderItems;
    private OrderStatus orderStatus;

    @Override
    public String toString() {
        String itens = "";

        for (OrderItem item : orderItems) {
            itens += "\n" + item.toString();
        }

        return "Pedido: " +
                "\nCódigo: " + id +
                "\n\nStatus: " + orderStatus +
                "\n " + itens;
    }
}
