package com.orlandoxavier.notification.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderItem implements Serializable {

    private String id;
    private String name;
    private int quantity;
    private String note;

    @Override
    public String toString() {
        return "" +
                "\nProduto: " + name +
                "\nQtd: " + quantity +
                "\nObs: " + note;
    }
}
