package com.orlandoxavier.notification.listener;

import com.orlandoxavier.notification.config.RabbitMQConfig;
import com.orlandoxavier.notification.model.Email;
import com.orlandoxavier.notification.model.Order;
import com.orlandoxavier.notification.model.OrderStatus;
import com.orlandoxavier.notification.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Component
public class NotificationListener {

    final MailService mailService;

    @RabbitListener(queues = RabbitMQConfig.NOTIFICATION_QUEUE)
    public void listener(Order order) {
        handleOrder(order);
    }

    private void handleOrder(Order order) {
        if (order == null || order.getOrderStatus() == null) {
            return;
        }

        if (order.getOrderStatus().getValue().equals(OrderStatus.DONE.getValue())) {
            LocalDateTime now = LocalDateTime.now();

            Email email = new Email();
            email.setSubject("Order DONE - " + now);
            email.setText(order.toString());

            mailService.sendMail(email);
            System.out.println("1 e-mail foi enviado às " + now + ".\nPedido [" + order.getId() + "]");
        }
    }
}
